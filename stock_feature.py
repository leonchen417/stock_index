import argparse
import yfinance as yf

# stock_symbols = ["BYND"]
ap = argparse.ArgumentParser()
ap.add_argument(
    "-s",
    "--stock_symbol",
    nargs="+",
    required=True,
    help="list of stock symbol seperated by space",
)
args = vars(ap.parse_args())
stock_symbols = args["stock_symbol"]
look_backs = ["1y", "2y", "5y"]
# dow ticker 'DJI'
# nasdaq ticker 'NDX'
# snp 500 'SPY'
# NYSE 'NYA'
dow = yf.Ticker("DJI")
nasdaq = yf.Ticker("NDX")
sp500 = yf.Ticker("SPY")
nyse = yf.Ticker("NYA")


for symbol in stock_symbols:
    stock = yf.Ticker(symbol)
    for look_back in look_backs:
        stock_his = stock.history(period=look_back)
        stock_to_dow = stock_his.corrwith(dow.history(period=look_back))
        stock_to_nasdaq = stock_his.corrwith(nasdaq.history(period=look_back))
        stock_to_sp500 = stock_his.corrwith(sp500.history(period=look_back))
        stock_to_nyse = stock_his.corrwith(nyse.history(period=look_back))
        print(f"For a period of {look_back}")
        print(f"Stock symbol {symbol} has correlation with Dow: {stock_to_dow}")
        print(f"Stock symbol {symbol} has correlation with NASDAQ: {stock_to_nasdaq}")
        print(f"Stock symbol {symbol} has correlation with SP500: {stock_to_sp500}")
        print(f"Stock symbol {symbol} has correlation with NYSE: {stock_to_nyse}")
