import ipdb
import yfinance as yf
import argparse
from millify import millify
import matplotlib.pyplot as plt

# stock_to_watch = ["BYND"]
# stock_to_watch = ["DBX"]
# objective -> run this code to regression check for 停損 and 停利 to ensure not to be washed away by volitility
# for example : 30% and 60%
ap = argparse.ArgumentParser()
ap.add_argument(
    "-s",
    "--stock_symbol",
    nargs="+",
    required=True,
    help="list of stock symbol seperated by space",
)
args = vars(ap.parse_args())
stock_to_watch = args["stock_symbol"]


# Valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
def check_stop_loss_percentage(stock, period, stop_loss_percent):
    # get -30% of each Close price as stop_loss_price
    # iterate through all Close price to find anytime with Close price lower then given loss_price, and seek for only future ones.
    breaking_stop_res = {}
    history = stock.history(period=period)
    for date, row in history.iterrows():
        close_price = row["Close"]
        stop_loss_price = close_price * (1 - stop_loss_percent / 100)
        df_filt = (history["Low"] < stop_loss_price) & (history.index.date > date)
        down_break_stop_loss_price = history[df_filt]
        if len(down_break_stop_loss_price) > 0:
            key = str(date).split(" ")[0] + "_cp_" + str(close_price)
            breaking_stop_res[key] = down_break_stop_loss_price
            # {"buy_in_date":some_date, "buy_at_closed_price": close_price, "wash_out_times": down_break_stop_loss_price}
        else:
            continue
    return breaking_stop_res


# increase stop_loss_percent 1 by 1, until len(down_break_stop_loss_price) is 0


def main():
    for symbol in stock_to_watch:
        stock = yf.Ticker(symbol)
        print(f'{symbol} has beta of {stock.get_info()["beta"]}')
        # a dictionary with key of stop perct, value of breaking instance
        xs = []
        ys = []
        stop_loss_break_dict = {}
        for stop_loss_pct in range(20, 76):
            res = check_stop_loss_percentage(stock, "2y", stop_loss_pct)
            if not res:
                break
            stop_loss_break_dict[stop_loss_pct] = res
            xs.append(stop_loss_pct)
            ys.append(len(res))
        print(
            f"for you to fully tolerate all risk points of {symbol}, you need {max(stop_loss_break_dict)} percentage stop loss"
        )
        plt.scatter(xs, ys)
        plt.xlabel("percentage % of stop loss")
        plt.ylabel("number of days will break the stop loss")
        plt.title(f"{symbol} stop loss risk level")
        plt.show()


if __name__ == "__main__":
    main()
