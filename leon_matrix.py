import pdb
import argparse
import yfinance as yf
from millify import millify


# stock_to_watch = ["MSFT", "WORK", "SPOT", "SQ", "TSLA", "TWOU"]
# objective -> run this matrix amount time of interested

ap = argparse.ArgumentParser()
ap.add_argument(
    "-s",
    "--stock_symbol",
    nargs="+",
    required=True,
    help="list of stock symbol seperated by space",
)
args = vars(ap.parse_args())
stock_to_watch = args["stock_symbol"]


def evaluate_quarterly_balance_sheet(stock):
    stock_qt_balancesheet = stock.quarterly_balance_sheet
    latest_qtbs = stock_qt_balancesheet[stock_qt_balancesheet.columns[0]]
    total_cur_asset = latest_qtbs["Total Current Assets"]
    total_cur_liab = latest_qtbs["Total Current Liabilities"]
    asset_minus_liab = total_cur_asset - total_cur_liab
    liab_devid_by_asset = total_cur_liab / total_cur_asset
    print(f"in the last quaterly balancesheet of {stock_qt_balancesheet.columns[0]}")
    print(f"Total Curr Asset : {millify(total_cur_asset,precision=2)}")
    print(f"Total Curr Liability : {millify(total_cur_liab,precision=2)}")
    print(f"Curr Asset - Curr Liab is: {millify(asset_minus_liab,precision=2)}")
    print(f"Curr liability / asset ratio is: {liab_devid_by_asset}")
    # TODO: some patent search for the stock
    return (
        latest_qtbs,
        total_cur_asset,
        total_cur_liab,
        liab_devid_by_asset,
        asset_minus_liab,
    )


def evaluate_income_statement(stock):
    stock_annual_income_statement = stock.financials
    # rev
    print("Revenue")
    revenue = stock_annual_income_statement.loc["Total Revenue"]
    print(revenue.apply(millify, args=(2,)))
    print("Net income")
    net_income = stock_annual_income_statement.loc["Net Income"]
    print(net_income.apply(millify, args=(2,)))
    # EBIT
    print("EBIT - this will distort if huge depreciation")
    ebit = stock_annual_income_statement.loc["Ebit"]
    print(ebit.apply(millify, args=(2,)))
    # net income / total revenue -> how efficient the company is (estimate)
    print("net incom / total revenue ratio (profit efficiency)")
    profit_efficiency = (
        stock_annual_income_statement.loc["Net Income"]
        / stock_annual_income_statement.loc["Total Revenue"]
    )
    print(profit_efficiency)
    TTM_income_statement = stock_annual_income_statement[
        stock_annual_income_statement.columns[0]
    ]
    total_operation_expense = TTM_income_statement["Total Operating Expenses"]
    return revenue, net_income, ebit, profit_efficiency, total_operation_expense


def growth_evaluate(stock):
    stock_annual_income_statement = stock.financials
    # rev growth
    rev_list = [i for i in stock_annual_income_statement.loc["Total Revenue"]]
    rev_growth = [i / rev_list[-1] for i in rev_list]
    print(f"Annual Revenue growth rate {rev_growth}")
    # net income growth
    net_income = [i for i in stock_annual_income_statement.loc["Net Income"]]
    net_income_growth = [i / net_income[-1] for i in net_income]
    print(f"Annual Net Income growth rate {net_income_growth}")
    # sales expense growth rate
    sale_expense = [
        i for i in stock_annual_income_statement.loc["Selling General Administrative"]
    ]
    printable_salese_expense = [millify(i, precision=2) for i in sale_expense]
    print(f"Annual Sales Expense: {printable_salese_expense}")
    sales_exp_growth = [i / sale_expense[-1] for i in sale_expense]
    print(f"Annual Sales Expense growth rate {sales_exp_growth}")
    # rnd expense growth rate
    rnd_expense = [i for i in stock_annual_income_statement.loc["Research Development"]]
    printable_rnd_expense = [millify(i, precision=2) if i else 0 for i in rnd_expense]
    print(f"Annual RnD Expense: {printable_rnd_expense}")
    rnd_exp_growth = [i / rnd_expense[-1] for i in rnd_expense if all(rnd_expense)]
    print(f"Annual RnD Expense growth rate: {rnd_exp_growth}")
    # sales/ rnd ratio (prefer rnd, have product sales itself)
    sales_devided_by_rnd_exp = [
        i[1] / (i[0] + 0.0001)
        for i in zip(rnd_expense, sale_expense)
        if all(rnd_expense) and all(sale_expense)
    ]
    print(f"Annual Sales/RnD expense ratio: {sales_devided_by_rnd_exp}")
    # sales expense growth rate / rnd expense growth rate (prefer high rnd)
    # if the company maintain no income due to sales/rnd new expense (good sign)
    return (
        rev_list,
        rev_growth,
        net_income,
        net_income_growth,
        sale_expense,
        sales_exp_growth,
        rnd_expense,
        rnd_exp_growth,
        sales_devided_by_rnd_exp,
    )


def calc_asset_to_dream_ratio(
    stock,
):  # takes 2 stocks, 1 target - 1 compare to seek ratio justification
    # stock_asset_price = (total asset - total liabilities=Total Stockholder Equity) / total outstanding shares
    # dream_to_asset_ratio = curr_stock price / stock_asset_price
    stock_qt_balancesheet = stock.quarterly_balance_sheet
    latest_qtbs = stock_qt_balancesheet[stock_qt_balancesheet.columns[0]]
    stock_holder_equity = latest_qtbs["Total Stockholder Equity"]
    share_outstanding = stock.info["sharesOutstanding"]
    stock_institution_own_ratio = stock.info["heldPercentInstitutions"]
    stock_insider_own_ratio = stock.info["heldPercentInsiders"]
    print(
        f"Total share outstanding is {millify(share_outstanding,precision=2)}, {stock_institution_own_ratio} of it own by institute and {stock_insider_own_ratio} own by insider"
    )
    stock_price_asset_principle = stock_holder_equity / share_outstanding
    last_3_trading_close_price = [i for i in stock.history().iloc[-3::]["Close"]]
    MA03 = sum(last_3_trading_close_price) / len(last_3_trading_close_price)
    dream_to_asset_price_ratio = (MA03) / stock_price_asset_principle
    print(
        f"Out of price of {MA03}, the asset account for {stock_price_asset_principle} with Stock price being {dream_to_asset_price_ratio} time of asset"
    )
    return (
        stock_institution_own_ratio,
        stock_insider_own_ratio,
        stock_holder_equity,
        stock_price_asset_principle,
        MA03,
        dream_to_asset_price_ratio,
    )


def calc_how_much_attension_stock_has(stock):
    # check trading volumn, compare it with Rev
    # check for institution ownership %
    average_volume_3m = stock.info["averageVolume"]
    volume = stock.info["volume"]
    beta = stock.info["beta"]  # 1 means equal to market, less then 1 is stable
    print(
        f"Has average Volume of {millify(average_volume_3m,precision=2)}, and Volum of {millify(volume,precision=2)}"
    )
    print(
        f"Has volitility index Beta of {beta}, 1 means equal to market, less is stable"
    )
    return average_volume_3m, volume, beta


def main():
    for symbol in stock_to_watch:
        print(
            f"===================for stock {symbol} ==========================================="
        )
        stock = yf.Ticker(symbol)
        _1, _2, _3, _4, curr_asset_minus_liab = evaluate_quarterly_balance_sheet(stock)
        _1, _2, _3, _4, twelve_month_op_expense = evaluate_income_statement(stock)
        growth_evaluate(stock)
        print(
            f"for stock {symbol}, the positive curr asset can burn {curr_asset_minus_liab/twelve_month_op_expense} year"
        )
        calc_asset_to_dream_ratio(stock)
        calc_how_much_attension_stock_has(stock)


if __name__ == "__main__":
    main()
